# screen ^C c
unbind ^C
bind ^C new-window
unbind c
bind c new-window

# detach ^D d
unbind ^D
bind ^D detach

# displays *
unbind *
bind * list-clients

# next ^@ ^N sp n
unbind ^@
bind ^@ next-window
unbind ^N
bind ^N next-window
unbind " "
bind " " next-window
unbind n
bind n next-window

# title A
unbind A
bind A command-prompt "rename-window %%"

# other ^A
unbind ^A
bind ^A last-window

# prev ^H ^P p ^?
unbind ^H
bind ^H previous-window
unbind ^P
bind ^P previous-window
unbind p
bind p previous-window
unbind BSpace
bind BSpace previous-window

# windows ^W w
unbind ^W
bind ^W list-windows
unbind w
bind w list-windows

# quit \
unbind '\'
bind '\' confirm-before "kill-server"

# kill K k
unbind K
bind K confirm-before "kill-window"
unbind k
bind k confirm-before "kill-window"

# redisplay ^L l
unbind ^L
bind ^L refresh-client
unbind l
bind l refresh-client

# " windowlist -b
unbind '"'
bind '"' choose-window

# lower repeat-time so that after you switch to a new pane
# you don't have to wait before inputing a key combo.
set -g repeat-time 100
set -g default-terminal "screen-256color"
#set -g default-terminal "screen"
set -g history-limit 10000

#new -s default -n emacs ~/bin/myemacs
#split-window -d -h
#new-window -d -n mutt mutt
#select-window -t 1
#split-window -d -h -p 25 khal interactive
#new -s default
#new-window -d -n shell0
#new-window -d
#new-window -d
#select-window -t 0
#select-pane -t 1

set -g default-shell $SHELL
set -g default-command "reattach-to-user-namespace -l ${SHELL}"

# when copying things in tmux, put them in the system paste buffer as well
#bind-key -t emacs-copy M-w copy-pipe "reattach-to-user-namespace pbcopy"
#bind-key -t emacs-copy M-w copy-pipe "pbcopy"
bind-key -T copy-mode M-w send-keys -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"

# terminal.app steals pgup/dn
#bind-key -t emacs-copy M-v page-up
#bind-key -t emacs-copy C-v page-down
bind-key -T copy-mode M-v send-keys -X page-up
bind-key -T copy-mode C-v send-keys -X page-down

#### COLOUR (Solarized 256)

# default statusbar colors
#set-option -g status-bg colour235 #base02
#set-option -g status-fg colour136 #yellow
#set-option -g status-attr default

# default window title colors
#set-window-option -g window-status-fg colour244 #base0
#set-window-option -g window-status-bg default
#set-window-option -g window-status-attr dim

# active window title colors
#set-window-option -g window-status-current-fg colour166 #orange
#set-window-option -g window-status-current-bg default
#set-window-option -g window-status-current-attr bright

# pane border
#set-option -g pane-border-fg colour235 #base02
#set-option -g pane-active-border-fg colour240 #base01

# message text
#set-option -g message-bg colour235 #base02
#set-option -g message-fg colour166 #orange

# pane number display
#set-option -g display-panes-active-colour colour33 #blue
#set-option -g display-panes-colour colour166 #orange

# clock
#set-window-option -g clock-mode-colour colour64 #green

# bell
set-window-option -g window-status-bell-style fg=colour235,bg=colour160 #base02, red

set -g mouse on

# try and disable the stupid alternate screen
set -ga terminal-overrides ',xterm*:smcup@:rmcup@'
set -ga terminal-overrides ',screen*:smcup@:rmcup@'

setw -g automatic-rename on

# auto config file reload
bind R source-file ~/.tmux.conf \; display-message "Config reloaded..."

set-option -g status on
set-option -g status-interval 2
set-option -g status-justify "centre"
set-option -g status-left-length 60
set-option -g status-right-length 90
set-option -g status-left "#(~/.tmux/tmux-powerline/powerline.sh left)"
set-option -g status-right "#(~/.tmux/tmux-powerline/powerline.sh right)"

# make session list more powerline-y
set-window-option -g window-status-current-format "#[fg=colour39, bg=colour238]\uE0B2#I:#W\uE0B0"

# don't allow tmux to change th session name
#set-option -g allow-rename off

set -g status-bg colour238
set -g status-fg colour31